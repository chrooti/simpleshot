**********
Simpleshot
**********

Screenshot utility for X. No fancy features, sane dependencies (libxcb, libturbojpeg).

Usage
#####
| simpleshot 
| -d display name (default: $DISPLAY)
| -f fill color as r,g,b (default: 0,0,0)
| -o output file name (default: screen.jpg)
| -s screen number (default: 0)

Why another screenshot tool?
############################
I simply couldn't find a screenshot tool with both sane dependencies (maim has *nine* for instance) and 
the capability of getting multi screen setups correctly (without noise in the image). So here we go.

N.B. it doesn't account for rotation or other randr features (yet!)

Build
#####
::

    git clone $(THIS REPO)
    make rel
    make build

The executable will be in build/simpleshot.
