.PHONY: debug release build run memcheck clean format

CC=clang
CXX=clang++
LDFLAGS=-fuse-ld=lld 
BUILD_DIR=build

debug:
	mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR) && CC=$(CC) CXX=$(CXX) LDFLAGS=$(LDFLAGS) cmake -GNinja -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_BUILD_TYPE=Debug ../

rel:
	mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR) && CC=$(CC) CXX=$(CXX) LDFLAGS=$(LDFLAGS) cmake -GNinja -DCMAKE_BUILD_TYPE=Release ../

build:
	cmake --build $(BUILD_DIR)

run: build
	./$(BUILD_DIR)/simpleshot

memcheck: build
	valgrind ./$(BUILD_DIR)/simpleshot

clean:
	rm -rf $(BUILD_DIR)

format:
	clang-format -i simpleshot.c
