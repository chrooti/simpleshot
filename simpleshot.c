#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <turbojpeg.h>

#include <xcb/randr.h>
#include <xcb/xcb.h>

int main(int argc, char **argv) {

    /* Parse options (no error checking here) */

    char display_name[255] = {'\0'};
    int screen_num = 0;
    char file_name[4096] = "screen.jpg";
    int jpeg_quality = 95;
    unsigned char fill_color[3] = {0, 0, 0};

    int opt;
    while ((opt = getopt(argc, argv, "d:f:o:")) != -1) {
        switch (opt) {

        // display name (default $DISPLAY)
        case 'd':
            strncpy(display_name, optarg, 255);
            break;

        // fill color in rgb (default "0,0,0")
        case 'f': {
            char *color = strtok(optarg, ",");
            fill_color[0] = strtoul(color, NULL, 10);
            color = strtok(NULL, ",");
            fill_color[1] = strtoul(color, NULL, 10);
            color = strtok(NULL, ",");
            fill_color[2] = strtoul(color, NULL, 10);
        } break;

        // output file name (default "screen.jpg")
        case 'o':
            strncpy(file_name, optarg, 4096);
            break;

        // jpeg quality (default: 95)
        case 'q':
            jpeg_quality = strtoul(optarg, NULL, 10);
            break;

        // screen number (default 0)
        case 's':
            screen_num = strtoul(optarg, NULL, 10);
            break;
        }
    }

    /* Get Xorg screen sizes and image */

    xcb_connection_t *xcb_connection;
    if (display_name[0] == '\0') {
        xcb_connection = xcb_connect(NULL, &screen_num);
    } else {
        xcb_connection = xcb_connect(display_name, &screen_num);
    }
    int err = xcb_connection_has_error(xcb_connection);
    if (err > 0) {
        xcb_disconnect(xcb_connection);
        fprintf(stderr, "Couldn't connect to the display\n");
        exit(-1);
    }

    const xcb_setup_t *xcb_setup = xcb_get_setup(xcb_connection);
    xcb_screen_t *screen = xcb_setup_roots_iterator(xcb_setup).data;

    int display_width = screen->width_in_pixels;
    int display_height = screen->height_in_pixels;

    xcb_window_t window = screen->root;

    xcb_get_image_cookie_t image_c = xcb_get_image(xcb_connection, XCB_IMAGE_FORMAT_Z_PIXMAP, window, 0, 0,
                                                   display_width, display_height, UINT32_MAX);
    xcb_get_image_reply_t *image = xcb_get_image_reply(xcb_connection, image_c, NULL);
    if (image == NULL) {
        xcb_disconnect(xcb_connection);
        fprintf(stderr, "Couldn't get a screenshot of the display\n");
        exit(-1);
    }

    /* Build a map of included pixels */

    // get randr resources
    xcb_randr_get_screen_resources_cookie_t xcb_resources_c = xcb_randr_get_screen_resources(xcb_connection, window);
    xcb_randr_get_screen_resources_reply_t *xcb_resources =
        xcb_randr_get_screen_resources_reply(xcb_connection, xcb_resources_c, NULL);
    if (xcb_resources == NULL) {
        fprintf(stderr, "Couldn't get screen resources\n");
        free(image);
        xcb_disconnect(xcb_connection);
        exit(-1);
    }

    // send requests for each connected hardware screen info
    int crtc_size = xcb_randr_get_screen_resources_crtcs_length(xcb_resources);
    xcb_randr_crtc_t *crtcs = xcb_randr_get_screen_resources_crtcs(xcb_resources);
    xcb_randr_get_crtc_info_cookie_t *xcb_crtcs_info_c = malloc(crtc_size * sizeof(xcb_randr_get_crtc_info_cookie_t));
    for (int i = 0; i < crtc_size; i++) {
        xcb_crtcs_info_c[i] = xcb_randr_get_crtc_info(xcb_connection, crtcs[i], 0);
    }
    free(xcb_resources);

    // init the pixelmap with the default color
    int pixelmap_size = display_height * 3 * display_width;
    unsigned char *pixelmap = malloc(pixelmap_size * sizeof(unsigned char));
    for (int i = 0; i < pixelmap_size; i += 3) {
        pixelmap[i] = fill_color[0];
        pixelmap[i + 1] = fill_color[1];
        pixelmap[i + 2] = fill_color[2];
    }

    // fill the pixelmap according to the output
    uint8_t *image_data = xcb_get_image_data(image);
    for (int i = 0; i < crtc_size; i++) {
        xcb_randr_get_crtc_info_reply_t *crtc_info =
            xcb_randr_get_crtc_info_reply(xcb_connection, xcb_crtcs_info_c[i], NULL);
        if (crtc_info == NULL) {
            continue;
        }

        if (crtc_info->mode == 0) {
            free(crtc_info);
            continue;
        }

        int h = crtc_info->y * display_width;
        int h_end = h + crtc_info->height * display_width;

        for (; h < h_end; h += display_width) {
            int pixelmap_h = h * 3;
            int image_h = pixelmap_h + h;

            unsigned char *pixelmap_row = pixelmap + (pixelmap_h);
            uint8_t *image_row = image_data + image_h;

            int w = crtc_info->x;
            int w_end = w + crtc_info->width;

            for (; w < w_end; w++) {
                // the pixelmap is RGB while the xcb-image is BRGX
                int pixelmap_w = 3 * w;
                int image_w = pixelmap_w + w;

                pixelmap_row[pixelmap_w] = image_row[image_w + 2];
                pixelmap_row[pixelmap_w + 1] = image_row[image_w + 1];
                pixelmap_row[pixelmap_w + 2] = image_row[image_w];
            }
        }

        free(crtc_info);
    }
    free(xcb_crtcs_info_c);
    free(image);
    xcb_disconnect(xcb_connection);

    /* Compress the image */

    tjhandle jpeg_compressor = tjInitCompress();

    unsigned char *jpeg_buffer = NULL;
    unsigned long jpeg_buffer_size = 0;

    tjCompress2(jpeg_compressor, pixelmap, display_width, 0, display_height, TJPF_RGB, &jpeg_buffer, &jpeg_buffer_size,
                TJSAMP_444, jpeg_quality, TJFLAG_FASTDCT);

    tjDestroy(jpeg_compressor);
    free(pixelmap);

    FILE *output = fopen(file_name, "wb");
    if (output == NULL) {
        fprintf(stderr, "Couldn't open output file\n");
        goto cleanup_output_error;
    }
    fwrite(jpeg_buffer, sizeof(unsigned char), jpeg_buffer_size, output);
    fclose(output);

    /* Free everything */

cleanup_output_error:

    tjFree(jpeg_buffer);
}
